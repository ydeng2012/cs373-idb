ifeq ($(shell uname), Darwin)          # Apple
    PYTHON   := python3
else ifeq ($(shell uname -p), unknown) # Windows
    PYTHON   := python
else                                   # UTCS
    PYTHON   := python3
endif


db:
	$(PYTHON) models.py
	$(PYTHON) create_db.py

test:
	$(PYTHON) tests.py