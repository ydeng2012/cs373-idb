from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",
                                                       'postgres://postgres:password@db:5432/bookdb')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)

link = db.Table('link',
                db.Column('author_id', db.Integer, db.ForeignKey('author.id')),
                db.Column('book_id', db.Integer, db.ForeignKey('book.id'))
                )


class Author(db.Model):
  """
  Authors have the following attributes
  author     (the author's name)
  src        (a link to a picture of the author)
  birthday   (the author's date of birth)
  hometown   (the author's hometown)
  education  (the college that the author attended)
  link       (a link to the author's website)
  id         (the id of the author in the database)
  works      (a list of Book objects that the author is linked to)
  """
  __tablename__ = 'author'

  author = db.Column(db.String(256), nullable=False)
  src = db.Column(db.String(256), nullable=False)
  birthday = db.Column(db.String(256), nullable=False)
  hometown = db.Column(db.String(256), nullable=False)
  education = db.Column(db.String(256), nullable=False)
  link = db.Column(db.String(256), nullable=False)
  id = db.Column(db.Integer, primary_key=True)
  works = db.relationship('Book', secondary='link', backref='authors')


class Publisher(db.Model):
  """
  Publishers have the following attributes
  name      (the publisher's name)
  src       (a link to a picture of the publisher's logo)
  phone     (the publisher's phone number)
  location  (the city the publisher is based in)
  date      (the year the publisher was founded)
  founder   (the publisher's founder)
  link      (a link to the publisher's website)
  id        (the id of the publisher in the database)
  books     (a list of Book objects that the publisher is linked to)
  """
  __tablename__ = 'publisher'

  name = db.Column(db.String(256), nullable=False)
  src = db.Column(db.String(256), nullable=False)
  phone = db.Column(db.String(256), nullable=False)
  location = db.Column(db.String(256), nullable=False)
  date = db.Column(db.String(256), nullable=False)
  founder = db.Column(db.String(256), nullable=False)
  link = db.Column(db.String(256), nullable=False)
  id = db.Column(db.Integer, primary_key=True)
  books = db.relationship('Book', backref='publisher')


class Book(db.Model):
  """
  Books have the following attributes
  title         (the book's title)
  src           (a link to a picture of the book's cover)
  summary       (a short summary of the book)
  ISBN          (the book's ISBN)
  publish_date  (the date the book was published)
  page          (the book's length in pages)
  language      (the book's language)
  link          (a link with more info about the book)
  id            (the id of the book in the database)
  publisher_id  (the id of the book's publisher in the database)
  publisher     (the Publisher object that the book is linked to)
  authors       (a list of Author objects that the book is linked to)
  """
  __tablename__ = 'book'

  title = db.Column(db.String(256), nullable=False)
  src = db.Column(db.String(256), nullable=False)
  summary = db.Column(db.String(65536), nullable=False)
  ISBN = db.Column(db.String(256), nullable=False)
  publish_date = db.Column(db.String(256), nullable=False)
  page = db.Column(db.String(256), nullable=False)
  language = db.Column(db.String(256), nullable=False)
  link = db.Column(db.String(256), nullable=False)
  id = db.Column(db.Integer, primary_key=True)
  publisher_id = db.Column(db.Integer, db.ForeignKey('publisher.id'))


db.create_all()
