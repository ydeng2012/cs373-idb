import os
import sys
import unittest
import warnings
from sqlalchemy import exc as sa_exc
from models import db, Author, Publisher, Book
from random import randint


class DBTestCases(unittest.TestCase):
  def test_author_1(self):
    # author of 0 books
    id = str(db.session.query(Author).count())
    s = Author(author='', src='', birthday='', hometown='', education='',
               link='', id=id)
    db.session.add(s)
    db.session.commit()

    r = db.session.query(Author).filter_by(id=id).one()
    self.assertEqual(str(r.id), id)

    db.session.query(Author).filter_by(id=id).delete()
    db.session.commit()

  def test_author_2(self):
    # author of 1 book
    auth_id = str(db.session.query(Author).count())
    book_id = str(db.session.query(Book).count())
    s = Author(author='', src='', birthday='', hometown='', education='',
               link='', id=auth_id)
    t = Book(title='', src='', summary='', ISBN='', publish_date='',
             page='', language='', link='', id=book_id)
    s.works.append(t)
    db.session.add(s)
    db.session.add(t)
    db.session.commit()

    r = db.session.query(Author).filter_by(id=auth_id).one()
    self.assertEqual(str(r.works[0].id), book_id)

    with warnings.catch_warnings():
      warnings.simplefilter("ignore", category=sa_exc.SAWarning)
      s.works = []
      db.session.query(Author).filter_by(id=auth_id).delete()
      db.session.query(Book).filter_by(id=book_id).delete()
      db.session.commit()

  def test_author_3(self):
    # author of many books
    auth_id = str(db.session.query(Author).count())
    book_id = db.session.query(Book).count()
    s = Author(author='', src='', birthday='', hometown='', education='',
               link='', id=auth_id)
    db.session.add(s)
    for i in range(0, 100):
      t = Book(title='', src='', summary='', ISBN='', publish_date='',
               page='', language='', link='', id=str(book_id + i))
      s.works.append(t)
      db.session.add(t)
    db.session.commit()

    r = db.session.query(Author).filter_by(id=auth_id).one()
    self.assertEqual(len(r.works), 100)

    with warnings.catch_warnings():
      warnings.simplefilter("ignore", category=sa_exc.SAWarning)
      s.works = []
      db.session.query(Author).filter_by(id=auth_id).delete()
      for i in range(0, 100):
        db.session.query(Book).filter_by(id=str(book_id + i)).delete()
      db.session.commit()

  def test_author_4(self):
    # author of 1 book
    auth_id = str(db.session.query(Author).count())
    book_id = str(db.session.query(Book).count())
    s = Author(author='Donald Trump', src='Does not exist',
               birthday='1946', hometown='New York City, United States', education='So smart he doesnt need it',
               link='', id=auth_id)
    t = Book(title='How to be President', src='None', summary='2016 Stuff', ISBN='18364621',
             publish_date='November 2016',
             page='100', language='en', link='N/A', id=book_id)
    s.works.append(t)
    db.session.add(s)
    db.session.add(t)
    db.session.commit()

    r = db.session.query(Author).filter_by(id=auth_id).one()
    self.assertEqual(str(r.works[0].id), book_id)

    with warnings.catch_warnings():
      warnings.simplefilter("ignore", category=sa_exc.SAWarning)
      s.works = []
      db.session.query(Author).filter_by(id=auth_id).delete()
      db.session.query(Book).filter_by(id=book_id).delete()
      db.session.commit()

  def test_author_5(self):
    # author of many books
    auth_id = str(db.session.query(Author).count())
    book_id = db.session.query(Book).count()
    s = Author(author='Justin Beaver', src='https://www.gstatic.com/tv/thumb/persons/174909/174909_v9_bb.jpg',
               birthday='January 1st, 1900', hometown='London, England', education='University of Phoenix',
               link='https://www.goodreads.com/author/show/1077326.J_K_Rowling', id=auth_id)
    db.session.add(s)
    for i in range(0, 7):
      t = Book(title='Bustin Jieber Book Number ' + str(i + 1),
               src='https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1574535986l/26032825._SY475_.jpg',
               summary='A book in the sernes', ISBN='3457983598', publish_date='February 23, 1927',
               page='453', language='en', link='https://www.goodreads.com/', id=str(book_id + i))
      s.works.append(t)
      db.session.add(t)
    db.session.commit()

    r = db.session.query(Author).filter_by(id=auth_id).one()
    self.assertEqual(len(r.works), 7)

    with warnings.catch_warnings():
      warnings.simplefilter("ignore", category=sa_exc.SAWarning)
      s.works = []
      db.session.query(Author).filter_by(id=auth_id).delete()
      for i in range(0, 7):
        db.session.query(Book).filter_by(id=str(book_id + i)).delete()
      db.session.commit()

  def test_author_6(self):
    # author of many books
    auth_id = str(db.session.query(Author).count())
    book_id = db.session.query(Book).count()
    s = Author(author='Justin Beaver', src='https://www.gstatic.com/tv/thumb/persons/174909/174909_v9_bb.jpg',
               birthday='January 1st, 1900', hometown='London, England', education='University of Phoenix',
               link='https://www.goodreads.com/author/show/1077326.J_K_Rowling', id=auth_id)
    db.session.add(s)
    for i in range(0, 100):
      t = Book(title='Bustin Jieber Book Number ' + str(i + 1),
               src='https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1574535986l/26032825._SY475_.jpg',
               summary='A book in the sernes', ISBN='3457983598', publish_date='February 23, 1927',
               page='453', language='en', link='https://www.goodreads.com/', id=str(book_id + i))
      s.works.append(t)
      db.session.add(t)
    db.session.commit()

    r = db.session.query(Author).filter_by(id=auth_id).one()
    self.assertEqual(len(r.works), 100)

    with warnings.catch_warnings():
      warnings.simplefilter("ignore", category=sa_exc.SAWarning)
      s.works = []
      db.session.query(Author).filter_by(id=auth_id).delete()
      for i in range(0, 100):
        db.session.query(Book).filter_by(id=str(book_id + i)).delete()
      db.session.commit()

  def test_author_7(self):
    # Nonexistent author
    self.assertEqual(len(db.session.query(Author).filter_by(author='?').all()), 0)

  def test_publisher_1(self):
    # publisher of 0 books
    id = str(db.session.query(Publisher).count())
    s = Publisher(name='', src='', phone='', location='', date='', founder='',
                  link='', id=id)
    db.session.add(s)
    db.session.commit()

    r = db.session.query(Publisher).filter_by(id=id).one()
    self.assertEqual(str(r.id), id)

    db.session.query(Publisher).filter_by(id=id).delete()
    db.session.commit()

  def test_publisher_2(self):
    # publisher of 1 book
    pub_id = str(db.session.query(Publisher).count())
    book_id = str(db.session.query(Book).count())
    s = Publisher(name='', src='', phone='', location='', date='', founder='',
                  link='', id=pub_id)
    t = Book(title='', src='', summary='', ISBN='', publish_date='',
             page='', language='', link='', id=book_id)
    s.books.append(t)
    db.session.add(s)
    db.session.add(t)
    db.session.commit()

    r = db.session.query(Publisher).filter_by(id=pub_id).one()
    self.assertEqual(str(r.books[0].id), book_id)

    with warnings.catch_warnings():
      warnings.simplefilter("ignore", category=sa_exc.SAWarning)
      s.books = []
      db.session.query(Publisher).filter_by(id=pub_id).delete()
      db.session.query(Book).filter_by(id=book_id).delete()
      db.session.commit()

  def test_publisher_3(self):
    # publisher of many books
    pub_id = str(db.session.query(Publisher).count())
    book_id = db.session.query(Book).count()
    s = Publisher(name='', src='', phone='', location='', date='', founder='',
                  link='', id=pub_id)
    db.session.add(s)
    for i in range(0, 100):
      t = Book(title='', src='', summary='', ISBN='', publish_date='',
               page='', language='', link='', id=str(book_id + i))
      s.books.append(t)
      db.session.add(t)
    db.session.commit()

    r = db.session.query(Publisher).filter_by(id=pub_id).one()
    self.assertEqual(len(r.books), 100)

    with warnings.catch_warnings():
      warnings.simplefilter("ignore", category=sa_exc.SAWarning)
      s.books = []
      db.session.query(Publisher).filter_by(id=pub_id).delete()
      for i in range(0, 100):
        db.session.query(Book).filter_by(id=str(book_id + i)).delete()
      db.session.commit()

  def test_publisher_4(self):
    # publisher of 1 book
    pub_id = str(db.session.query(Publisher).count())
    book_id = str(db.session.query(Book).count())
    s = Publisher(name='Random Stuff Here', src='This isnt even a real image link LOL',
                  phone='273495162', location='Mysterious',
                  date='This does not fit a date format for testing purposes', founder='Spongebob Squarepants',
                  link='www.thislinktakesyoutonowhere.com', id=pub_id)
    t = Book(title='0192847562', src='No image just cuz', summary='This book does not exist',
             ISBN='use letter here instead of numbers for testing purposes', publish_date='fake date for testing',
             page='one hundred and fifty', language='Elvish', link='Not a link for testing purposes', id=book_id)
    s.books.append(t)
    db.session.add(s)
    db.session.add(t)
    db.session.commit()

    r = db.session.query(Publisher).filter_by(id=pub_id).one()
    self.assertEqual(str(r.books[0].id), book_id)

    with warnings.catch_warnings():
      warnings.simplefilter("ignore", category=sa_exc.SAWarning)
      s.books = []
      db.session.query(Publisher).filter_by(id=pub_id).delete()
      db.session.query(Book).filter_by(id=book_id).delete()
      db.session.commit()

  def test_publisher_5(self):
    # publisher of many books
    pub_id = str(db.session.query(Publisher).count())
    book_id = db.session.query(Book).count()
    s = Publisher(name='Nintendo Inc', src='N/A', phone='N/A', location='Japan', date='1889', founder='N/A',
                  link='www.nintendo.com', id=pub_id)
    db.session.add(s)
    for i in range(0, 100):
      t = Book(title='Super Mario Bros. ' + str(i + 1), src='N\A', summary='N\A', ISBN='N\A', publish_date='N\A',
               page='N\A', language='N\A', link='N\A', id=str(book_id + i))
      s.books.append(t)
      db.session.add(t)
    db.session.commit()

    r = db.session.query(Publisher).filter_by(id=pub_id).one()
    self.assertEqual(len(r.books), 100)

    with warnings.catch_warnings():
      warnings.simplefilter("ignore", category=sa_exc.SAWarning)
      s.books = []
      db.session.query(Publisher).filter_by(id=pub_id).delete()
      for i in range(0, 100):
        db.session.query(Book).filter_by(id=str(book_id + i)).delete()
      db.session.commit()

  def test_publisher_6(self):
    # publisher of many books
    pub_id = str(db.session.query(Publisher).count())
    book_id = db.session.query(Book).count()
    s = Publisher(name='Nintendo Inc', src='N/A', phone='N/A', location='Japan', date='1889', founder='N/A',
                  link='www.nintendo.com', id=pub_id)
    db.session.add(s)
    for i in range(0, 1000):
      t = Book(title='Super Mario Bros. ' + str(i + 1), src='N\A', summary='N\A', ISBN='N\A', publish_date='N\A',
               page='N\A', language='N\A', link='N\A', id=str(book_id + i))
      s.books.append(t)
      db.session.add(t)
    db.session.commit()

    r = db.session.query(Publisher).filter_by(id=pub_id).one()
    self.assertEqual(len(r.books), 1000)

    with warnings.catch_warnings():
      warnings.simplefilter("ignore", category=sa_exc.SAWarning)
      s.books = []
      db.session.query(Publisher).filter_by(id=pub_id).delete()
      for i in range(0, 1000):
        db.session.query(Book).filter_by(id=str(book_id + i)).delete()
      db.session.commit()

  def test_publisher_7(self):
    # Nonexistent publisher
    self.assertEqual(len(db.session.query(Publisher).filter_by(name='?').all()), 0)

  def test_book_1(self):
    # book
    id = str(db.session.query(Book).count())
    s = Book(title='', src='', summary='', ISBN='', publish_date='', page='',
             language='', link='', id=id)
    db.session.add(s)
    db.session.commit()

    r = db.session.query(Book).filter_by(id=id).one()
    self.assertEqual(str(r.id), id)

    db.session.query(Book).filter_by(id=id).delete()
    db.session.commit()

  def test_book_2(self):
    # book with 1 author and 1 publisher
    book_id = str(db.session.query(Book).count())
    pub_id = str(db.session.query(Publisher).count())
    auth_id = str(db.session.query(Author).count())
    s = Book(title='', src='', summary='', ISBN='', publish_date='', page='',
             language='', link='', id=book_id)
    t = Publisher(name='', src='', phone='', location='', date='', founder='',
                  link='', id=pub_id)
    u = Author(author='', src='', birthday='', hometown='', education='',
               link='', id=auth_id)
    s.publisher = t
    s.authors.append(u)
    db.session.add(s)
    db.session.add(t)
    db.session.add(u)
    db.session.commit()

    r = db.session.query(Book).filter_by(id=book_id).one()
    self.assertEqual(str(r.publisher.id), pub_id)
    self.assertEqual(str(r.authors[0].id), auth_id)

    with warnings.catch_warnings():
      warnings.simplefilter("ignore", category=sa_exc.SAWarning)
      s.publisher = None
      s.authors = []
      db.session.query(Publisher).filter_by(id=pub_id).delete()
      db.session.query(Author).filter_by(id=auth_id).delete()
      db.session.query(Book).filter_by(id=book_id).delete()
      db.session.commit()

  def test_book_3(self):
    # book with many authors and 1 publisher
    book_id = str(db.session.query(Book).count())
    pub_id = str(db.session.query(Publisher).count())
    auth_id = db.session.query(Author).count()
    s = Book(title='', src='', summary='', ISBN='', publish_date='', page='',
             language='', link='', id=book_id)
    db.session.add(s)
    t = Publisher(name='', src='', phone='', location='', date='', founder='',
                  link='', id=pub_id)
    s.publisher = t
    db.session.add(t)
    for i in range(0, 100):
      u = Author(author='', src='', birthday='', hometown='', education='',
                 link='', id=str(auth_id + i))
      s.authors.append(u)
      db.session.add(u)
    db.session.commit()

    r = db.session.query(Book).filter_by(id=book_id).one()
    self.assertEqual(str(r.publisher.id), pub_id)
    self.assertEqual(len(r.authors), 100)

    with warnings.catch_warnings():
      warnings.simplefilter("ignore", category=sa_exc.SAWarning)
      s.publisher = None
      s.authors = []
      db.session.query(Publisher).filter_by(id=pub_id).delete()
      for i in range(0, 100):
        db.session.query(Author).filter_by(id=str(auth_id + i)).delete()
      db.session.query(Book).filter_by(id=book_id).delete()
      db.session.commit()

  def test_book_4(self):
    # book with many authors and 1 publisher
    book_id = str(db.session.query(Book).count())
    pub_id = str(db.session.query(Publisher).count())
    auth_id = db.session.query(Author).count()
    s = Book(title='', src='', summary='', ISBN='', publish_date='', page='',
             language='', link='', id=book_id)
    db.session.add(s)
    t = Publisher(name='', src='', phone='', location='', date='', founder='',
                  link='', id=pub_id)
    s.publisher = t
    db.session.add(t)
    for i in range(0, 50):
      u = Author(author='', src='', birthday='', hometown='', education='',
                 link='', id=str(auth_id + i))
      s.authors.append(u)
      db.session.add(u)
    db.session.commit()

    r = db.session.query(Book).filter_by(id=book_id).one()
    self.assertEqual(str(r.publisher.id), pub_id)
    self.assertEqual(len(s.authors), 50)

    with warnings.catch_warnings():
      warnings.simplefilter("ignore", category=sa_exc.SAWarning)
      s.publisher = None
      s.authors = []
      db.session.query(Publisher).filter_by(id=pub_id).delete()
      for i in range(0, 100):
        db.session.query(Author).filter_by(id=str(auth_id + i)).delete()
      db.session.query(Book).filter_by(id=book_id).delete()
      db.session.commit()

  def test_book_5(self):
    # book with many authors and 1 publisher
    book_id = str(db.session.query(Book).count())
    pub_id = str(db.session.query(Publisher).count())
    auth_id = db.session.query(Author).count()
    s = Book(title='', src='', summary='', ISBN='', publish_date='', page='',
             language='', link='', id=book_id)
    db.session.add(s)
    t = Publisher(name='', src='', phone='', location='', date='', founder='',
                  link='', id=pub_id)
    s.publisher = t
    db.session.add(t)
    for i in range(0, 20):
      u = Author(author='', src='', birthday='', hometown='', education='',
                 link='', id=str(auth_id + i))
      s.authors.append(u)
      db.session.add(u)
    db.session.commit()

    r = db.session.query(Book).filter_by(id=book_id).one()
    self.assertEqual(str(r.publisher.id), pub_id)
    self.assertEqual(len(s.authors), 20)

    with warnings.catch_warnings():
      warnings.simplefilter("ignore", category=sa_exc.SAWarning)
      s.publisher = None
      s.authors = []
      db.session.query(Publisher).filter_by(id=pub_id).delete()
      for i in range(0, 20):
        db.session.query(Author).filter_by(id=str(auth_id + i)).delete()
      db.session.query(Book).filter_by(id=book_id).delete()
      db.session.commit()

  def test_book_6(self):
    # book with many authors and 1 publisher
    book_id = str(db.session.query(Book).count())
    pub_id = str(db.session.query(Publisher).count())
    auth_id = db.session.query(Author).count()
    s = Book(title='', src='', summary='', ISBN='', publish_date='', page='',
             language='', link='', id=book_id)
    db.session.add(s)
    t = Publisher(name='', src='', phone='', location='', date='', founder='',
                  link='', id=pub_id)
    s.publisher = t
    db.session.add(t)
    for i in range(0, 150):
      u = Author(author='', src='', birthday='', hometown='', education='',
                 link='', id=str(auth_id + i))
      s.authors.append(u)
      db.session.add(u)
    db.session.commit()

    r = db.session.query(Book).filter_by(id=book_id).one()
    self.assertEqual(str(r.publisher.id), pub_id)
    self.assertEqual(len(s.authors), 150)

    with warnings.catch_warnings():
      warnings.simplefilter("ignore", category=sa_exc.SAWarning)
      s.publisher = None
      s.authors = []
      db.session.query(Publisher).filter_by(id=pub_id).delete()
      for i in range(0, 150):
        db.session.query(Author).filter_by(id=str(auth_id + i)).delete()
      db.session.query(Book).filter_by(id=book_id).delete()
      db.session.commit()

  def test_book_7(self):
    # Nonexistent book
    self.assertEqual(len(db.session.query(Book).filter_by(title='?').all()), 0)

  def test_book_8(self):
    # many books
    book_id = str(db.session.query(Book).count())
    for i in range(0, 100):
      s = Book(title='', src='', summary='', ISBN='', publish_date='', page='',
               language='', link='', id=str(int(book_id) + i))
      db.session.add(s)
    db.session.commit()

    for i in range(0, 100):
      r = db.session.query(Book).filter_by(id=str(int(book_id) + i)).one()
      self.assertEqual(str(r.id), str(int(book_id) + i))

    with warnings.catch_warnings():
      warnings.simplefilter("ignore", category=sa_exc.SAWarning)
      for i in range(0, 100):
        db.session.query(Book).filter_by(id=str(int(book_id) + i)).delete()
      db.session.commit()


if __name__ == '__main__':  # pragma no cover
  unittest.main()
