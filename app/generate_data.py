"""
This script pulls 100 books, authors and publishers and places them in
books.json, authors.json, and publishers.json respectively.
"""

import json
import random
import requests


MAX_RESULTS = 5
GOOGLE_BOOKS_URL = "https://www.googleapis.com/books/v1/volumes?q={}&maxResults={}"

def generate_books():
    with open("authors.json", "r") as f:
        authors = json.load(f)
    with open("publishers.json", "r") as f:
        publishers = json.load(f)

    author_ids = {
        author["author"]: author["id"]
        for author in authors["Authors"]
    }
    publisher_ids = {
        publisher["name"]: publisher["id"]
        for publisher in publishers["Publishers"]
    }

    taken_books = set()

    books = {"Books": []}
    for author_name, author_id in author_ids.items():
        response = requests.get(GOOGLE_BOOKS_URL.format("inauthor:" + author_name, MAX_RESULTS))
        search_results = json.loads(response.text)["items"]
        for book in search_results:
            if book["volumeInfo"]["title"] not in taken_books:
                book_json = {
                    "title": book["volumeInfo"]["title"],
                    "src": book["volumeInfo"].get("imageLinks", {"smallThumbnail": "#"})["smallThumbnail"],
                    "summary": book["volumeInfo"].get("description", "N/A"),
                    "ISBN": book["volumeInfo"].get("industryIdentifiers", [{"identifier": "N/A"}])[0]["identifier"],
                    "publish_date": book["volumeInfo"].get("publishedDate", "N/A"),
                    "page": book["volumeInfo"].get("pageCount", "N/A"),
                    "language": book["volumeInfo"]["language"],
                    "link": "https://www.goodreads.com",
                    "id": len(books["Books"]),
                    "publisher_id": publisher_ids.get(book["volumeInfo"]["title"], random.randint(0, len(publisher_ids) - 1)),
                    "author_ids": [author_id]
                }
                taken_books.add(book["volumeInfo"]["title"])
                books["Books"].append(book_json)

    for publisher_name, publisher_id in publisher_ids.items():
        response = requests.get(GOOGLE_BOOKS_URL.format("inpublisher:" + publisher_name, MAX_RESULTS))
        search_results = json.loads(response.text)["items"]
        for book in search_results:
            if book["volumeInfo"]["title"] not in taken_books:
                book_json = {
                    "title": book["volumeInfo"]["title"],
                    "src": book["volumeInfo"].get("imageLinks", {"smallThumbnail": "#"})["smallThumbnail"],
                    "summary": book["volumeInfo"].get("description", "N/A"),
                    "ISBN": book["volumeInfo"].get("industryIdentifiers", [{"identifier": "N/A"}])[0]["identifier"],
                    "publish_date": book["volumeInfo"].get("publishedDate", "N/A"),
                    "page": book["volumeInfo"].get("pageCount", "N/A"),
                    "language": book["volumeInfo"]["language"],
                    "link": "https://www.goodreads.com",
                    "id": len(books["Books"]),
                    "publisher_id": publisher_id,
                    "author_ids": [author_ids.get(book["volumeInfo"].get("authors", ["Suzanne Collins"])[0], random.randint(0, len(author_ids) - 1))]
                }
                taken_books.add(book["volumeInfo"]["title"])
                books["Books"].append(book_json)

    with open("books.json", "w") as f:
        json.dump(books, f, indent=4)


def main():
    generate_books()


if __name__ == "__main__":
    main()

