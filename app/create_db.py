import json
from models import app, db, Book, Publisher, Author


def load_json(filename):
  with open(filename) as file:
    jsn = json.load(file)
    file.close()
  return jsn


__publishers = []


def create_publishers():
  for book in db.session.query(Book).all():
    book.publisher = None
  db.session.query(Publisher).delete()
  pubs = load_json('./app/publishers.json')
  for pub in pubs['Publishers']:
    name = pub['name']
    src = pub['src']
    phone = pub['phone']
    location = pub['location']
    date = pub['date']
    founder = pub['founder']
    link = pub['link']
    id = pub['id']
    new_pub = Publisher(name=name, src=src, phone=phone, location=location,
                        date=date, founder=founder, link=link, id=id)
    __publishers.append(new_pub)
    db.session.add(new_pub)
    db.session.commit()


__authors = []


def create_authors():
  for book in db.session.query(Book).all():
    book.authors = []
  db.session.query(Author).delete()
  auths = load_json('./app/authors.json')
  for auth in auths['Authors']:
    author = auth['author']
    src = auth['src']
    birthday = auth['birthday']
    hometown = auth['hometown']
    education = auth['education']
    link = auth['link']
    id = auth['id']
    new_auth = Author(author=author, src=src, birthday=birthday,
                      hometown=hometown, education=education, link=link, id=id)
    __authors.append(new_auth)
    db.session.add(new_auth)
    db.session.commit()


def create_books():
  db.session.query(Book).delete()
  books = load_json('./app/books.json')
  for book in books['Books']:
    title = book['title']
    src = book['src']
    summary = book['summary']
    ISBN = book['ISBN']
    publish_date = book['publish_date']
    page = book['page']
    language = book['language']
    link = book['link']
    id = book['id']
    publisher_id = book['publisher_id']
    author_ids = book['author_ids']
    new_book = Book(title=title, src=src, summary=summary, ISBN=ISBN,
                    publish_date=publish_date, page=page, language=language, link=link,
                    id=id, publisher=__publishers[int(publisher_id)])
    for id in author_ids:
      new_book.authors.append(__authors[int(id)])
    db.session.add(new_book)
    db.session.commit()


create_publishers()
create_authors()
create_books()
