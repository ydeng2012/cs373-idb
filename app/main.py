from flask import Flask, render_template, url_for, request, Response, jsonify, redirect
from flask_cors import CORS
from tests import DBTestCases
import unittest
import io
import contextlib
from create_db import db, Book, Publisher, Author
from sqlalchemy.orm.exc import NoResultFound
import requests

app = Flask(__name__, static_folder='../build', static_url_path='/')
CORS(app)
app.config['TEMPLATES_AUTO_RELOAD'] = True


@app.route('/')
@app.route('/books/')
@app.route('/publishers/')
@app.route('/about/')
@app.route('/authors/')
@app.route('/search/')
@app.route('/visualization/')
def index():
  return app.send_static_file('index.html')


@app.route('/books/bookdetail/<int:user_id>')
@app.route('/publishers/publisherdetail/<string:user_id>')
@app.route('/authors/authordetail/<string:user_id>')
def data_detail(user_id):
  return app.send_static_file('index.html')


def get_book_dict(book):
  author_string = ', '.join([auth.author for auth in book.authors])
  return {"title": book.title, "src": book.src, "summary": book.summary, "author": author_string,
          "ISBN": book.ISBN, "publish_date": book.publish_date, "publisher": book.publisher.name,
          "page": book.page, "language": book.language, "link": book.link, "id": book.id}


def get_author_dict(author):
  works_string = ", ".join([work.title for work in author.works])
  return {"author": author.author, "src": author.src, "birthday": author.birthday, "hometown": author.hometown,
          "education": author.education, "works": works_string, "link": author.link, "id": author.id}


def get_publisher_dict(publisher):
  books_string = ", ".join([book.title for book in publisher.books])
  return {"name": publisher.name, "src": publisher.src, "phone": publisher.phone, "location": publisher.location,
          "date": publisher.date, "founder": publisher.founder, "books": books_string, "link": publisher.link,
          "id": publisher.id}


@app.route('/api/data/')
def data():
  response = []
  book_list = db.session.query(Book).all()
  for book in book_list:
    new_one = get_book_dict(book)
    response.append(new_one)
  new_list = {'books': response}
  return new_list


@app.route('/api/author/')
def author():
  response = []
  author_list = db.session.query(Author).all()
  for author in author_list:
    new_one = get_author_dict(author)
    response.append(new_one)
  new_list = {'authors': response}
  return new_list


@app.route('/api/publisher/')
def publisher():
  response = []
  publisher_list = db.session.query(Publisher).all()
  for publisher in publisher_list:
    new_one = get_publisher_dict(publisher)
    response.append(new_one)
  new_list = {'publishers': response}
  return new_list


@app.route('/api/author/<int:auth_id>/')
def author_by_ID_JSON(auth_id):
  if auth_id < 0 or auth_id >= db.session.query(Author).count():
    dict = {'code': 404, 'message': 'Author not found'}
  else:
    author = db.session.query(Author).filter_by(id=auth_id).one()
    dict = get_author_dict(author)
  return dict


@app.route('/api/author/name=<string:name>/')
def author_by_name_JSON(name):
  response = []
  authors = db.session.query(Author).filter(Author.author.ilike('%' + name + '%')).all()
  for author in authors:
    dict = get_author_dict(author)
    response.append(dict)
  return {'authors': response}


@app.route('/api/publisher/<int:pub_id>/')
def publisher_by_ID_JSON(pub_id):
  if pub_id < 0 or pub_id >= db.session.query(Publisher).count():
    dict = {'code': 404, 'message': 'Publisher not found'}
  else:
    publisher = db.session.query(Publisher).filter_by(id=pub_id).one()
    dict = get_publisher_dict(publisher)
  return dict


@app.route('/api/publisher/name=<string:name>/')
def publisher_by_name_JSON(name):
  response = []
  pubs = db.session.query(Publisher).filter(Publisher.name.ilike('%' + name + '%')).all()
  for pub in pubs:
    dict = get_publisher_dict(pub)
    response.append(dict)
  return {'publishers': response}


@app.route('/api/data/<int:book_id>/')
def book_by_ID_JSON(book_id):
  if book_id < 0 or book_id >= db.session.query(Book).count():
    dict = {'code': 404, 'message': 'Book not found'}
  else:
    book = db.session.query(Book).filter_by(id=book_id).one()
    dict = get_book_dict(book)
  return dict


@app.route('/api/data/ISBN=<string:isbn>/')
def book_by_ISBN_JSON(isbn):
  try:
    book = db.session.query(Book).filter_by(ISBN=isbn).one()
    dict = get_book_dict(book);
  except NoResultFound:
    dict = {'code': 404, 'message': 'Book not found'}
  return dict


@app.route('/api/data/name=<string:name>/')
def book_by_name_JSON(name):
  response = []
  books = db.session.query(Book).filter(Book.title.ilike('%' + name + '%')).all()
  for book in books:
    dict = get_book_dict(book)
    response.append(dict)
  return {'books': response}


@app.route('/api/testcase/')
def testcase():
  message = ""
  suite = unittest.TestLoader().loadTestsFromTestCase(DBTestCases)
  with io.StringIO() as buf:
    with contextlib.redirect_stdout(buf):
      unittest.TextTestRunner(stream=buf).run(suite)
      message = buf.getvalue()
  return jsonify(message)


@app.route('/vizdata/')
def viz_data():
  return {'dog_data': individual_data('dogs'), 'cat_data': individual_data('cats'),
          'dog_breed_data': breed_data('dog'), 'cat_breed_data': breed_data('cat')}


def individual_data(type):
  animals = requests.get('http://54.156.72.101/individual_' + type).json()['individual_' + type]
  breed_counts = {}
  gender_counts = {}
  breed_gender_counts = {}
  for animal in animals:
    breed = animal['breed']
    gender = animal['gender']
    breed_gender = ", ".join([breed, gender])
    breed_counts[breed] = breed_counts.get(breed, 0) + 1
    gender_counts[gender] = gender_counts.get(gender, 0) + 1
    breed_gender_counts[breed_gender] = breed_gender_counts.get(breed_gender, 0) + 1
  dict = {'breed_counts': breed_counts, 'gender_counts': gender_counts, 'breed_gender_counts': breed_gender_counts}
  return dict


def breed_data(type):
  breeds = requests.get('http://54.156.72.101/' + type + '_breeds').json()[type + '_breeds']
  temperament_counts = {}
  for breed in breeds:
    temperaments = breed['temperament'].split(", ")
    for temperament in temperaments:
      temperament_counts[temperament] = temperament_counts.get(temperament, 0) + 1
  dict = {'temperament_counts': temperament_counts}
  return dict


if __name__ == '__main__':
  app.run(host='0.0.0.0')
# pragma: no cover
