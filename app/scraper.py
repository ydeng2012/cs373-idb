import requests

URL = "https://www.googleapis.com/books/v1/volumes?q="
MAX_RESULTS = 1

term = input("Enter search term: ")
URL += term
URL += "&maxResults=" + str(MAX_RESULTS)

response = requests.get(URL)
json = response.text
print(json)

save = input("Would you like to save to a json file? (Y/N) ") == "Y"
if save:
    name = input("Enter file name (without extension): ")
    out = open(name + ".json", "w")
    out.write(json)
    out.close()
    print(name + ".json was created")