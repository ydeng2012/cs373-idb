<pre>
Group members:

Name                    eid             gitlab id           estimated completion time     actual completion time
----------------------------------------------------------------------------------------------------------------
Yin Deng                yd4459          ydeng2012                       hours                       hours
Tate Song               tls3763         tbobo                           hours                       hours
Ye Sheng                ys8672          shengye000                      hours                       hours
George Zhang            gz2835          gzhang1218                      hours                       hours
Connor Kounthapanya     clk2662         connor-kounthapanya             hours                       hours
</pre>


## Prerequisites

### Clone git repository
Clone the project onto your local machine:

```bash
git clone https://gitlab.com/ydeng2012/cs373-idb.git
```

Enter project folder cs373-idb:
```bash
cd cs373-idb
```

### Setup virtual environment
If not installed:

```bash
pip3 install virtualenv
```

In the project folder cs373-idb:

**On Windows:**

```bash
virtualenv venv
```

**On Mac:**

```bash
pyvenv venv
```

**On Linux:**

```bash
virtualenv -p python3 venv
```

### Activate/deactivate virtual environment
On Windows (assuming you’re using Git Bash):
```bash
. venv/Scripts/activate
(venv)
$
```
On Mac:
```bash
source venv/bin/activate
(venv) $
```

On Linux:
```bash
. venv/bin/activate
(venv) $
```

Deactivate:
```bash
(venv) 
$ deactivate
```

### Install flask in virtual environment
```bash
(venv) 
$ pip install flask 
```

**For Phase 3:**
Install Flask-SQLAlchemy

```bash
(venv) 
$ pip install Flask-SQLAlchemy
```

Install psycopg2

```bash
(venv) 
$ pip install psycopg2-binary
```

### Check if Node and npm are installed
```bash
node --version
npm --version
```

### Install Node JS (if not installed)
Refer to https://nodejs.org/en/ to install nodejs.

For Windows:
    Download and install 12.18.1 LTS

For Linux (Node.js v12.x):
```bash
# Using Ubuntu
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get install -y nodejs
```
Source: https://github.com/nodesource/distributions/blob/master/README.md

### Install create-react-app
Install create-react-app npm package globally. This will help to easily run the project and also build the source files easily. Use the following command to install create-react-app

```bash
npm install -g create-react-app
```

If in virtual environment (no need for global installation):

```bash
npm install create-react-app
```

## Installing npm packages and running

Install all the npm packages. Go into the project folder and type the following command to install all npm packages:

```bash
npm install
```

In order to run the application, type the following command:

```bash
npm start
```

The React application runs on **localhost:3000**

### Install Python requirements

```bash
pip3 install -r requirements.txt 
```

## Run Flask application locally

```bash
python3 main.py 
```

The Flask application/server runs on **localhost:5000**

## Run the Flask application in a server like gunicorn

We first do:
```bash
npm run-script build
```

This will make a build folder with all the static files for gunicorn to find, then run:

```bash
gunicorn -w 4 main:app
```

If the page does not reflect the changes, just reload the web page again and it should work.

## Deployment on GCP

### NEW: Automatic Deployment through GitLab CI

**Find setup instructions in [Wiki](https://gitlab.com/ydeng2012/cs373-idb/-/wikis/Hosting)**

Deploy changes automatically to GCP by pushing to the `deployment` branch.

### Manual deployment on GCP using Google Cloud Shell

**While signed into your gmail account, create a project on App Engine:**
* From the navigation menu, select "App Engine".
* From the dropdown menu next to the navigation menu, create a new project.

**Clone repo and deploy:**
* Click on the Google Cloud Shell icon in the top right hand corner of the console.
* Clone repository from GitLab:
```bash
https://gitlab.com/ydeng2012/cs373-idb.git
cd cs373-idb
```
* Setup and activate virtual environment from steps above.
* Install Flask and create-react-app from the steps above.
* Install npm packages and requirements from pip3:
```bash
npm install 
```
```bash
pip3 install -r requirements.txt 
```
**Note that you should re-run from here if you update the repo on GCP through a git pull or anything**
* Build project files:
```bash
npm run-script build
```
* Deploy application from app.yaml:
```bash
gcloud app deploy
```
Note: You might need to answer some prompts to continue. The process will take quite a while, so just wait.

* Access application:

You'll see something like this in the output once it's deployed:
```bash
Deployed service [default] to [https://cs373-idb-281323.uc.r.appspot.com]
You can stream logs from the command line by running:
  $ gcloud app logs tail -s default
To view your application in the web browser run:
  $ gcloud app browse
```
