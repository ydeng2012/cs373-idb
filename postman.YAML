{
  "openapi": "3.0.0",
  "info": {
    "version": "1.0.0",
    "title": "Bookmaster",
    "license": {
      "name": "MIT"
    }
  },
  "servers": [
    {
      "url": "http://www.bookmaster.me/api"
    }
  ],
  "paths": {
    "/data/": {
      "get": {
        "summary": "All books",
        "operationId": "listBook",
        "tags": [
          "book"
        ],
        "parameters": [
        ],
        "responses": {
          "200": {
            "description": "Details about the books",
            "headers": {
              "x-next": {
                "description": "A link to the next page of responses",
                "schema": {
                  "type": "string"
                }
              }
            },
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Book"
                  }
                }
              }
            }
          }
        }
      }
    },
    "/data/name={name}/": {
      "get": {
        "summary": "Books by title",
        "operationId": "listBook",
        "tags": [
          "book"
        ],
        "parameters": [
          {
            "name": "name",
            "description": "Title of the book",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Details about the books",
            "headers": {
              "x-next": {
                "description": "A link to the next page of responses",
                "schema": {
                  "type": "string"
                }
              }
            },
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Book"
                  }
                }
              }
            }
          }
        }
      }
    },
    "/data/ISBN={isbn}/": {
      "get": {
        "summary": "Book by ISBN",
        "operationId": "listBook",
        "tags": [
          "book"
        ],
        "parameters": [
          {
            "name": "id",
            "description": "ISBN of the book",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Details about a book",
            "headers": {
              "x-next": {
                "description": "A link to the next page of responses",
                "schema": {
                  "type": "string"
                }
              }
            },
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Book"
                }
              }
            }
          },
          "default": {
            "description": "unexpected error",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Error"
                }
              }
            }
          }
        }
      }
    },
    "/data/{id}/": {
      "get": {
        "summary": "Book by ID",
        "operationId": "listBook",
        "tags": [
          "book"
        ],
        "parameters": [
          {
            "name": "id",
            "description": "ID of the book",
            "required": true,
            "schema": {
              "type": "integer",
              "format": "int32"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Details about a book",
            "headers": {
              "x-next": {
                "description": "A link to the next page of responses",
                "schema": {
                  "type": "string"
                }
              }
            },
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Book"
                }
              }
            }
          },
          "default": {
            "description": "unexpected error",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Error"
                }
              }
            }
          }
        }
      }
    },
    "/author/": {
      "get": {
        "summary": "All authors",
        "operationId": "listAuthor",
        "tags": [
          "book"
        ],
        "parameters": [
        ],
        "responses": {
          "200": {
            "description": "Details about the authors",
            "headers": {
              "x-next": {
                "description": "A link to the next page of responses",
                "schema": {
                  "type": "string"
                }
              }
            },
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Author"
                  }
                }
              }
            }
          }
        }
      }
    },
    "/author/name={name}/": {
      "get": {
        "summary": "Authors by name",
        "operationId": "listAuthor",
        "tags": [
          "book"
        ],
        "parameters": [
          {
            "name": "name",
            "description": "Name of the author",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Details about the authors",
            "headers": {
              "x-next": {
                "description": "A link to the next page of responses",
                "schema": {
                  "type": "string"
                }
              }
            },
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Author"
                  }
                }
              }
            }
          }
        }
      }
    },
    "/author/{id}/": {
      "get": {
        "summary": "Author by ID",
        "operationId": "listAuthor",
        "tags": [
          "author"
        ],
        "parameters": [
          {
            "name": "id",
            "description": "ID of the author",
            "required": true,
            "schema": {
              "type": "integer",
              "format": "int32"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Details about an author",
            "headers": {
              "x-next": {
                "description": "A link to the next page of responses",
                "schema": {
                  "type": "string"
                }
              }
            },
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Author"
                }
              }
            }
          },
          "default": {
            "description": "unexpected error",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Error"
                }
              }
            }
          }
        }
      }
    },
    "/publisher/": {
      "get": {
        "summary": "All publishers",
        "operationId": "listPublisher",
        "tags": [
          "book"
        ],
        "parameters": [
        ],
        "responses": {
          "200": {
            "description": "Details about the publishers",
            "headers": {
              "x-next": {
                "description": "A link to the next page of responses",
                "schema": {
                  "type": "string"
                }
              }
            },
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Publisher"
                  }
                }
              }
            }
          }
        }
      }
    },
    "/publisher/name={name}/": {
      "get": {
        "summary": "Publishers by name",
        "operationId": "listPublisher",
        "tags": [
          "book"
        ],
        "parameters": [
          {
            "name": "name",
            "description": "Name of the publisher",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Details about the publishers",
            "headers": {
              "x-next": {
                "description": "A link to the next page of responses",
                "schema": {
                  "type": "string"
                }
              }
            },
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Publisher"
                  }
                }
              }
            }
          }
        }
      }
    },
    "/publisher/{id}/": {
      "get": {
        "summary": "Publisher by ID",
        "operationId": "listPublisher",
        "tags": [
          "publisher"
        ],
        "parameters": [
          {
            "name": "id",
            "description": "ID of the publisher",
            "required": true,
            "schema": {
              "type": "integer",
              "format": "int32"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Details about a publisher",
            "headers": {
              "x-next": {
                "description": "A link to the next page of responses",
                "schema": {
                  "type": "string"
                }
              }
            },
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Publisher"
                }
              }
            }
          },
          "default": {
            "description": "unexpected error",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Error"
                }
              }
            }
          }
        }
      }
    }
  },
  "components": {
    "schemas": {
      "Book": {
        "type": "object",
        "properties": {
          "title": {
            "type": "string"
          },
          "src": {
            "type": "string"
          },
          "summary": {
            "type": "string"
          },
          "author": {
            "type": "string"
          },
          "ISBN": {
            "type": "string"
          },
          "publish_date": {
            "type": "string"
          },
          "publisher": {
            "type": "string"
          },
          "page": {
            "type": "string"
          },
          "language": {
            "type": "string"
          },
          "link": {
            "type": "string"
          },
          "id": {
            "type": "string"
          }
        },
        "required": [
          "title",
          "src",
          "summary",
          "author",
          "ISBN",
          "publish_date",
          "publisher",
          "page",
          "language",
          "link",
          "id"
        ]
      },
      "Author": {
        "type": "object",
        "properties": {
          "author": {
            "type": "string"
          },
          "src": {
            "type": "string"
          },
          "birthday": {
            "type": "string"
          },
          "hometown": {
            "type": "string"
          },
          "education": {
            "type": "string"
          },
          "works": {
            "type": "string"
          },
          "link": {
            "type": "string"
          },
          "id": {
            "type": "string"
          }
        },
        "required": [
          "author",
          "src",
          "birthday",
          "hometown",
          "education",
          "works",
          "link",
          "id"
        ]
      },
      "Publisher": {
        "type": "object",
        "properties": {
          "name": {
            "type": "string"
          },
          "src": {
            "type": "string"
          },
          "phone": {
            "type": "string"
          },
          "location": {
            "type": "string"
          },
          "date": {
            "type": "string"
          },
          "founder": {
            "type": "string"
          },
          "link": {
            "type": "string"
          },
          "books": {
            "type": "string"
          },
          "id": {
            "type": "string"
          }
        },
        "required": [
          "name",
          "src",
          "phone",
          "location",
          "date",
          "founder",
          "link",
          "books",
          "id"
        ]
      },
      "Error": {
        "type": "object",
        "required": [
          "code",
          "message"
        ],
        "properties": {
          "code": {
            "type": "integer",
            "format": "int32"
          },
          "message": {
            "type": "string"
          }
        }
      }
    }
  }
}