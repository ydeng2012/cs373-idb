import React from 'react'
import {Nav, Navbar} from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import {LinkContainer} from 'react-router-bootstrap'

export default class Navigation extends React.Component {
    render() {
        return (
            <Navbar bg="dark" expand="lg" variant="dark" className="text-uppercase">
                <Navbar.Brand className="js-scroll-trigger" href="/">Book Master</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav"
                               className="text-uppercase font-weight-bold bg-primary text-white rounded">Menu</Navbar.Toggle>
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ml-auto">
                        <LinkContainer to="/">
                            <Nav.Link>Home</Nav.Link>
                        </LinkContainer>
                        <LinkContainer to="/books">
                            <Nav.Link>Books</Nav.Link>
                        </LinkContainer>
                        <LinkContainer to="/publishers">
                            <Nav.Link>Publishers</Nav.Link>
                        </LinkContainer>
                        <LinkContainer to="/authors">
                            <Nav.Link>Authors</Nav.Link>
                        </LinkContainer>
						<LinkContainer to="/search">
                            <Nav.Link>Search</Nav.Link>
                        </LinkContainer>
                        <LinkContainer to="/about">
                            <Nav.Link>About</Nav.Link>
                        </LinkContainer>
                        <LinkContainer to="/visualization">
                            <Nav.Link>Visualization</Nav.Link>
                        </LinkContainer>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        )
    }
}