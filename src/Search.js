import React from "react";
import {Link} from "react-router-dom";
import Highlighter from "react-highlight-words";
import { Helmet } from 'react-helmet'
import Favicon from 'react-favicon';

const TITLE = 'Book Master: Search'

class Search extends React.Component {
    constructor(props) {
        super(props);
        this.myRef = React.createRef()
        this.state = {
            books: [],
            authors: [],
            publishers: [],
            searchText: '',
			searchTextArray:[],
			booksSearch: [],
			authorsSearch: [],
			publishersSearch: []
        }
        this.searchData = this.searchData.bind(this)
    }

    componentDidMount() {
        fetch('/api/data').then(r => r.json()).then(data => {
            this.setState({books: data.books})
        })
        fetch('/api/author').then(r => r.json()).then(author => {
            this.setState({authors: author.authors})
        })
        fetch('/api/publisher').then(r => r.json()).then(publisher => {
            this.setState({publishers: publisher.publishers})
        })

    }

    searchData() {
        let input = document.getElementById("searchBar").value;
        this.myRef.current = input
        this.setState({
            searchText: input
        });
		
		if (input === ""){
			this.setState({booksSearch: []})
			this.setState({authorsSearch: []})
			this.setState({publishersSearch: []})
		}
		else{
			var inputSplit = input.split(" ");
			this.setState({
				searchTextArray: inputSplit
			});
			//search books
			let bookList = []
			for (let book of this.state.books) {
				var i;
				var x = true;
				for (i = 0; i < inputSplit.length; i++) {
					if (!((book.title.toLowerCase()).indexOf(inputSplit[i].toLowerCase()) >= 0)) {
						x = false;
						break;
					}
				} 
				if (x === true){
					bookList.push(book)	
				}
			}
			this.setState({booksSearch: bookList})
			
			//search authors
			let authorList = []
			for (let author of this.state.authors) {
				var i;
				var x = true;
				for (i = 0; i < inputSplit.length; i++) {
					if (!((author.author.toLowerCase()).indexOf(inputSplit[i].toLowerCase()) >= 0)) {
						x = false;
						break;
					}
				} 
				if (x === true){
					authorList.push(author)	
				}
			}
			this.setState({authorsSearch: authorList})
			
			//search publishers
			let publisherList = []
			for (let publisher of this.state.publishers) {
				var i;
				var x = true;
				for (i = 0; i < inputSplit.length; i++) {
					if (!((publisher.name.toLowerCase()).indexOf(inputSplit[i].toLowerCase()) >= 0)) {
						x = false;
						break;
					}
				} 
				if (x === true){
					publisherList.push(publisher)	
				}
			}
			this.setState({publishersSearch: publisherList})
		}
		
    }

    render() {
        const {books} = this.state.books
        const {publishers} = this.state.publishers
        const {authors} = this.state.authors
		var Highlight = require('react-highlighter');
		
        return (
            <div class="aboutpadding background-blue">
				<Helmet>
				  <title>{ TITLE }</title>
				</Helmet>
				<div>
				  <Favicon url="https://i.imgur.com/eoGvMtb.png" />
				</div>
                <div>
                    <h1 className='text-center'><b>Search:</b></h1>
                    <br/>
                    <div style={{display: 'flex', justifyContent: 'center'}}>
                        <img className="img-center"
                             src="https://vignette.wikia.nocookie.net/kingdomhearts/images/4/46/BookMasterFM.png/revision/latest?cb=20110824232556"
                             alt="not found"/>
                    </div>
                    <br/>
                    <div class="aboutcard" style={{display: 'flex', justifyContent: 'center'}}>
                        <input class="form-control form-control-lg" type="text" id="searchBar"
                               placeholder="Search a book, author, or publisher:"></input>
                        <button type="submit" class="btn btn-primary btn-lg" onClick={this.searchData}>Search
                        </button>
                    </div>
                    <br/>
                </div>

                <div>
                    <h2 className='text-center'><b>You searched: {this.myRef.current} </b></h2>
					<h5 className='text-center'>Number of results: {(this.state.booksSearch).length + (this.state.authorsSearch).length + 
						(this.state.publishersSearch).length}</h5>
					<br/>
					<div>
						{(this.state.booksSearch).map((book, i) =>
							<div class="borderdiv" key={i}>
								<Link to={{
									pathname: `/books/bookdetail/${book.id}`,
								}}>
									<h1><b>Book: </b>
										<Highlighter
											highlightClassName="YourHighlightClass"
											searchWords={this.state.searchTextArray}
											autoEscape={true}
											textToHighlight={book.title}
										  />
									</h1>									
								</Link>
								<p class="card-text"><b>Description:</b> {book.summary} </p>
								<p class="card-text"><b>Author:</b> {book.author} &emsp; <b>ISBN:</b> {book.ISBN} &emsp; <b>Release Date:</b> {book.publish_date} &emsp;
								<b>Publisher:</b> {book.publisher} &emsp; <b>Number of Pages:</b> {book.page} &emsp; <b>Language:</b> {book.language} </p>
								<br/>
							</div>
						)}
					</div>
					<div>
					{(this.state.authorsSearch).map((aut, i) =>
                                    <div class="borderdiv" key={i}>
                                        <Link to={{
                                            pathname: `/authors/authordetail/${aut.author}`,
                                        }}>
                                            <h1><b>Author: </b>
												<Highlighter
												highlightClassName="YourHighlightClass"
												searchWords={this.state.searchTextArray}
												autoEscape={true}
												textToHighlight={aut.author}
												/>
											</h1>
                                        </Link>
                                        <p class="card-text"><b>Birthday:</b> {aut.birthday} &emsp; <b>Hometown:</b> {aut.hometown} &emsp; <b>Education:</b> {aut.education}</p>
                                        <p class="card-text"><b>Known For:</b> {aut.works} </p>
										<br/>
                                    </div>
                     )}
					</div>
					<div>
					{(this.state.publishersSearch).map((publisher, i) =>
                                    <div class="borderdiv" key={i}>
                                        <Link to={{
                                            pathname: `/publishers/publisherdetail/${publisher.name}`,
                                        }}>
                                            <h1><b>Publisher: </b>
											<Highlighter
												highlightClassName="YourHighlightClass"
												searchWords={this.state.searchTextArray}
												autoEscape={true}
												textToHighlight={publisher.name}
												/>
											</h1>
                                        </Link>
                                        <p class="card-text"><b>Phone:</b> {publisher.phone} &emsp; <b>City:</b> {publisher.location} &emsp; <b>Founded:</b> {publisher.date} &emsp; <b>Founder:</b> {publisher.founder}</p>
										<br/>
                                    </div>
                                )}
					</div>
                </div>
            </div>
        )
    }

}

export default Search;

