import React from 'react'
import {Link} from "react-router-dom";
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import { Helmet } from 'react-helmet'
import Favicon from 'react-favicon';

const TITLE = 'Book Master: Publishers'

class Publishers extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            publishers: []
        }
    }

    componentDidMount() {
        fetch('/api/publisher').then(r => r.json()).then(publisher => {
            this.setState({publishers: publisher.publishers})
        })
    }

    render() {		
		function imageFormatter(cell, row) {
			return (
			<td style={{
				width: "200px",
				height: "200px",
				backgroundColorlor: "white",
				textAlign: "center",
				verticalAlign: "Alimiddle"
			}}>
				<img className="img" src={ cell } alt={"Image Not Found"}
					style={{maxHeight: '100%', maxWidth: '100%'}}/>
			</td>
									
			);
		}
		
		function publisherDetail(cell, row) {
			return (
					<Link to={{
						pathname: `/publishers/publisherdetail/${row.name}`,
					}}>{ cell }</Link>				
			);
		}
        const {publishers} = this.state
		const {columns} = {columns: [{
			  dataField: 'name',
			  text: 'Publisher',
			  sort: true,
			  formatter: publisherDetail
			}, {
			  dataField: 'src',
			  text: 'Logo',
			  sort: true,
			  formatter: imageFormatter,
			  searchable: false
			}, {
			  dataField: 'phone',
			  text: 'Phone Number',
			  sort: true,
			  searchable: false
			}, {
			  dataField: 'location',
			  text: 'HQ Location',
			  sort: true,
			  searchable: false
			}, {
			  dataField: 'date',
			  text: 'Founded',
			  sort: true,
			  searchable: false
			}, {
			  dataField: 'founder',
			  text: 'Founder',
			  sort: true,
			  searchable: false
			}, {
			  dataField: 'link',
			  text: 'Website',
			  sort: true,
			  hidden: true,
			  searchable: false
			}, {
			  dataField: 'id',
			  text: 'ID',
			  sort: true,
			  hidden: true,
			  searchable: false
			}		
		]}
		const { SearchBar, ClearSearchButton } = Search
        return (
            <div>
				<Helmet>
				  <title>{ TITLE }</title>
				</Helmet>
				<div>
				  <Favicon url="https://i.imgur.com/eoGvMtb.png" />
				</div>
                <h1 className="text-center">Publishers</h1>
				<h5 className="text-center">Hint: You can click on the column names to sort by that column information up or down alphanumerically.</h5>
                <div class="card-deck aboutcard">
					<div class="card">
						<div class="card-body">
							<a href="/books">Books</a>
						</div>
					</div>
					<div class="card">
						<div class="card-body">
							<p><b>Publishers</b></p>
						</div>
					</div>
					<div class="card">
						<div class="card-body">
							<a href="/authors">Authors</a>
						</div>
					</div>
				</div>
				<br/>
				<ToolkitProvider
				  keyField="id"
				  data={ publishers }
				  columns={ columns }
				  search
				  >
				  {
					props => (
					  <div>
						<div style={{display: 'flex', justifyContent: 'center'}}>
							<SearchBar { ...props.searchProps }/>
							<ClearSearchButton { ...props.searchProps } />
						</div> 
						<hr/>
						<BootstrapTable
							{...props.baseProps }
							striped
							pagination={ paginationFactory() }
						/>
					  </div>
					)
				  }
				</ToolkitProvider>
            </div>
        )
    }
}

export default Publishers;