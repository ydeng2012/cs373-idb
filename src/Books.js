import React from 'react'
import {Link} from 'react-router-dom';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import { Helmet } from 'react-helmet'
import Favicon from 'react-favicon';

const TITLE = 'Book Master: Books'

class Books extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            books: []
        }
    }

    componentDidMount() {
        fetch('/api/data').then(r => r.json()).then(data => {
            this.setState({books: data.books})
        })

    }

    render() {
        function imageFormatter(cell, row) {
            return (
                <img className="img" src={cell} alt={"Cover Not Found"}
                     style={{maxHeight: '100%', maxWidth: '100%'}}/>
            );
        }

        function bookDetail(cell, row) {
            return (
                <Link to={{
                    pathname: `/books/bookdetail/${row.id}`,
                }}>{cell}</Link>
            );
        }

        function authorDetail(cell, row) {
            return (
                <Link to={{
                    pathname: `/authors/authordetail/${row.author}`,
                }}>{cell}</Link>
            );
        }

        function publisherDetail(cell, row) {
            return (
                <Link to={{
                    pathname: `/publishers/publisherdetail/${row.publisher}`,
                }}>{cell}</Link>
            );
        }

        const {books} = this.state
        const {columns} = {
            columns: [{
                dataField: 'title',
                text: 'Title',
                sort: true,
                formatter: bookDetail
            }, {
                dataField: 'src',
                text: 'Front Cover',
                sort: true,
                formatter: imageFormatter,
				searchable: false
            }, {
                dataField: 'summary',
                text: 'Summary',
                sort: true,
				searchable: false
            }, {
                dataField: 'author',
                text: 'Author',
                sort: true,
                formatter: authorDetail
            }, {
                dataField: 'ISBN',
                text: 'ISBN',
                sort: true,
				searchable: false
            }, {
                dataField: 'publish_date',
                text: 'Release Date',
                sort: true,
				searchable: false
            }, {
                dataField: 'publisher',
                text: 'Publisher',
                sort: true,
                formatter: publisherDetail
            }, {
                dataField: 'page',
                text: 'Number of Pages',
                sort: true,
				searchable: false
            }, {
                dataField: 'language',
                text: 'Language',
                sort: true,
				searchable: false
            }, {
                dataField: 'link',
                text: 'Link',
                sort: true,
                hidden: true,
				searchable: false
            }, {
                dataField: 'id',
                text: 'ID',
                sort: true,
                hidden: true,
				searchable: false
            }
            ]
        }
		const { SearchBar, ClearSearchButton } = Search

        return (
            <div>
				<Helmet>
				  <title>{ TITLE }</title>
				</Helmet>
				<div>
				  <Favicon url="https://i.imgur.com/eoGvMtb.png" />
				</div>
                <h1 className="text-center">Books</h1>
                <h5 className="text-center">Hint: You can click on the column names to sort by that column information
                    up or down alphanumerically.</h5>
				<div class="card-deck aboutcard">
					<div class="card">
						<div class="card-body">
							<p><b>Books</b></p>
						</div>
					</div>
					<div class="card">
						<div class="card-body">
							<a href="/publishers">Publishers</a>
						</div>
					</div>
					<div class="card">
						<div class="card-body">
							<a href="/authors">Authors</a>
						</div>
					</div>
				</div>
				<br/>

				<ToolkitProvider
				  keyField="id"
				  data={ books }
				  columns={ columns }
				  search
				  >
				  {
					props => (
					  <div>
						<div style={{display: 'flex', justifyContent: 'center'}}>
							<SearchBar { ...props.searchProps }/>
							<ClearSearchButton { ...props.searchProps } />
						</div> 
						<hr/>
						<BootstrapTable
							{...props.baseProps }
							striped
							pagination={ paginationFactory() }
						/>
					  </div>
					)
				  }
				</ToolkitProvider>
				
            </div>
        )
    }
}


export default Books;