import React from 'react'
import { Link } from 'react-router-dom';

class Detail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
    }
	
	componentDidMount() {
        fetch('/books').then(data => {
            this.setState({data: this.props.location.state.book})
        })
    }

    render() {
		const {data} = this.state
        return (
            <div>   
			<p>{data.title}</p>
			<p>{data.summary}</p>
            </div>
        )
    }
}

export default Detail;