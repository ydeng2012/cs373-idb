import React from 'react';
import './index.css';
import { Button} from 'react-bootstrap';
import { Helmet } from 'react-helmet'
import Favicon from 'react-favicon';

const TITLE = 'Book Master'

class Landing extends React.Component{
	constructor(props) {
		super(props);
		this.scrollDiv = React.createRef();
		this.scrollDiv2 = React.createRef();
	}

	render(){	
		return (	
			<div>
				<Helmet>
				  <title>{ TITLE }</title>
				</Helmet>
				<div>
				  <Favicon url="https://i.imgur.com/eoGvMtb.png" />
				</div>
				<div className="bg">
					<br/>
					<div style={{display: 'flex', justifyContent: 'center'}}>
						<img className="img-center" src="https://i.imgur.com/cJ5kaFy.png" alt="not found"/>
					</div> 
					<br/>
					<div style={{display: 'flex', justifyContent: 'center'}}>
						<b><font size="7" color="#00FFFF">Book, Author & Publisher Content For <br/> 
								All Readers, Writers, and Bibliophiles.</font></b>
					</div> 
					<br/>
					<br/>
					<div style={{display: 'flex', justifyContent: 'center'}}>
						<Button variant="success" size="lg"
						  onClick={() => {
							this.scrollDiv.current.scrollIntoView({ behavior: 'smooth' });
						  }}> Click Here To Learn More </Button>
					</div> 
					<br/>
					<div style={{display: 'flex', justifyContent: 'center'}}>
						<Button variant="success" size="lg"
						  onClick={() => {
							this.scrollDiv2.current.scrollIntoView({ behavior: 'smooth' });
						  }}> Click Here To Find Books </Button>
					</div> 
				</div>
				
				<div ref={this.scrollDiv} className="aboutcard background-green">
					<br/>
					<b><h1 className='text-center'>About Book Master</h1></b>
					<br/>
					<div style={{display: 'flex', justifyContent: 'center'}}>
						<img className="img-center" src="https://vignette.wikia.nocookie.net/kingdomhearts/images/4/46/BookMasterFM.png/revision/latest?cb=20110824232556" alt="not found"/>	
					</div>        
					<br/>
					<h4 className='text-center'>
						Book Master is a UT-Austin student created website to help you look for fantastic books. 
						We provide all the information you need about the newest bestselling and obscure books 
						including a summary, number of pages, release dates, and more. Not only that, we specialize
						in quickly finding other books by that same author or publishing company that you might enjoy. 
						<br/>
						<br/>
						We are proud of the incredibly personalized service we offer. From quickly finding books and
						providing necessary details, our job is sure to deliver results customers will be satisfied with
						<br/>
						<br/>
						So whether you haven't read a book since high school, a bored person looking for a new hobby, 
						a daydreamer who wants a good story, or a erudite book addict, it matters not. We got your back!
						<br/>
					</h4>
					<br/>
				</div>				
				
				<div ref={this.scrollDiv2} className="aboutcard background-pink">
					<b><h1 className='text-center'>Find Books</h1></b>
					<br/>
					<h4 className='text-center'>
						Are you reading to find books? You can either go to our search page below, or click one of 
						the three links to search by either books, authors, or publishers.
					</h4>
					<br/>
					<div style={{display: 'flex', justifyContent: 'center'}}>
						<div class="text-center">
							<Button variant="btn btn-info" size="lg" href="/search"> Click Here To Search </Button>
						</div>
					</div> 

					<br/>
					
					<div class="card-deck">
						<div class="card">
								<img className="img" src="https://www.adazing.com/wp-content/uploads/2019/02/open-book-clipart-03.png" alt="Pic Not Found"
									 style={{maxHeight: '100%', maxWidth: '100%'}}/>
							<div class="card-body">
								<h5 class="card-title" className='text-center'>Books</h5>
								<p class="card-text" className = 'text-center'> Interested in finding the entire list of books
									we have on our website? Well, the place is just one click away below!</p>
								<div class="text-center">
								<Button variant="primary" size="lg" href="/books"> Books </Button>
								</div>
							</div>
						</div>
						
						<div class="card">
								<img className="img" src="https://www.differencebetween.info/sites/default/files/images/4/auth(1).jpg" alt="Pic Not Found"
									 style={{maxHeight: '100%', maxWidth: '100%'}}/>
							<div class="card-body">
								<h5 class="card-title" className='text-center'>Authors</h5>
								<p class="card-text" className = 'text-center'> Authors are the important to books, with every
									book's life beginning with a person creating words on a paper or computer.  </p>
								<div class="text-center">
								<Button variant="primary" size="lg" href="/authors"> Authors </Button>
								</div>
							</div>
						</div>
						
						<div class="card">
								<img className="img" src="https://cdn0.iconfinder.com/data/icons/3D_Cartoon_Icons_Pack/300/Microsoft_Office_Publisher.png" alt="Pic Not Found"
									 style={{maxHeight: '100%', maxWidth: '100%'}}/>
							<div class="card-body">
								<h5 class="card-title" className='text-center'>Publishers</h5>
								<p class="card-text" className = 'text-center'> From famous ones to Scholastic to less
									well known ones like Tor Books, you will find all kinds of publishers here.</p>
								<div class="text-center">
								<Button variant="primary" size="lg" href="/publishers"> Publishers </Button>
								</div>
							</div>
						</div>
						
					</div>							
					<br/>
					<br/>
					<div class="text-center">
						<Button variant="outline-secondary" size="lg" href="/about"> Click Here To Meet The Team </Button>
					</div>
					<br/>
					<div class="text-center">
						<Button variant="outline-secondary" size="lg" href="/visualization"> Click Here For Visualization </Button>
					</div>
					<br/>
					<br/>
				
				</div>
				

				
			</div>
		);
	}
}

export default Landing;