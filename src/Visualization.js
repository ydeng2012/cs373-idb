import React from 'react'
import BubbleChart from '@weknow/react-bubble-chart-d3';
import Typography from "@material-ui/core/Typography";
import Helmet from "react-helmet";
import Favicon from "react-favicon";

const TITLE = 'Book Master: Visualization'

export default class Visualization extends React.Component {

    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount() {
        fetch('/vizdata/').then(r => r.json()).then(data => {
            this.setState({
                dog_data: data.dog_data, // breed counts, breed gender counts, gender counts
                cat_data: data.cat_data,
                dog_breed_data: data.dog_breed_data,
                cat_breed_data: data.cat_breed_data
            })
        })
    }

    render() {
        const {dog_data, cat_data, dog_breed_data, cat_breed_data} = this.state
        let dog_res = []
        let cat_res = []
        let dog_temperament = []
        let cat_temperament = []
        for (let counts in dog_data) {
            if (counts === 'breed_counts') {
                for (let [key, val] of Object.entries(dog_data[counts])) {
                    let tmp = {label: key, value: val}
                    dog_res.push(tmp)
                }
            }
        }
        for (let counts in cat_data) {
            if (counts === 'breed_counts') {
                for (let [key, val] of Object.entries(cat_data[counts])) {
                    let tmp = {label: key, value: val}
                    cat_res.push(tmp)
                }
            }
        }
        for (let counts in dog_breed_data) {
            for (let [key, val] of Object.entries(dog_breed_data[counts])) {
                let tmp = {label: key, value: val}
                dog_temperament.push(tmp)
            }
        }
        for (let counts in cat_breed_data) {
            for (let [key, val] of Object.entries(cat_breed_data[counts])) {
                let tmp = {label: key, value: val}
                cat_temperament.push(tmp)
            }
        }
        dog_temperament = dog_temperament.sort((a, b) => {
            return a['value'] - b['value']
        }).reverse().slice(0, 10)
        cat_temperament = cat_temperament.sort((a, b) => {
            return a['value'] - b['value']
        }).reverse().slice(0, 10)

        return (
            <div style={{textAlign: 'center'}}>
                <Helmet>
                    <title>{TITLE}</title>
                </Helmet>
                <div>
                    <Favicon url="https://i.imgur.com/eoGvMtb.png"/>
                </div>
                <br/>
                <br/>
                <h2>
                    Top dogs being adopted
                </h2>
                <BubbleChart
                    graph={{
                        zoom: 1.1,
                    }}
                    width={1000}
                    height={1000}
                    padding={0} // optional value, number that set the padding between bubbles
                    showLegend={true} // optional value, pass false to disable the legend.
                    legendPercentage={20} // number that represent the % of with that legend going to use.
                    legendFont={{
                        family: 'Arial',
                        size: 12,
                        color: '#000',
                        weight: 'bold',
                    }}
                    valueFont={{
                        family: 'Arial',
                        size: 12,
                        color: '#fff',
                        weight: 'bold',
                    }}
                    labelFont={{
                        family: 'Arial',
                        size: 16,
                        color: '#fff',
                        weight: 'bold',
                    }}
                    data={dog_res}
                />
                <h2>
                    Top cats being adopted
                </h2>
                <BubbleChart
                    graph={{
                        zoom: 1.1,
                    }}
                    width={1000}
                    height={1000}
                    padding={0} // optional value, number that set the padding between bubbles
                    showLegend={true} // optional value, pass false to disable the legend.
                    legendPercentage={20} // number that represent the % of with that legend going to use.
                    legendFont={{
                        family: 'Arial',
                        size: 12,
                        color: '#000',
                        weight: 'bold',
                    }}
                    valueFont={{
                        family: 'Arial',
                        size: 12,
                        color: '#fff',
                        weight: 'bold',
                    }}
                    labelFont={{
                        family: 'Arial',
                        size: 16,
                        color: '#fff',
                        weight: 'bold',
                    }}
                    data={cat_res}
                />
                <h2>
                    Top 10 characteristics dogs have for adoption
                </h2>
                <BubbleChart
                    graph={{
                        zoom: 1.1,
                    }}
                    width={1000}
                    height={1000}
                    padding={0} // optional value, number that set the padding between bubbles
                    showLegend={true} // optional value, pass false to disable the legend.
                    legendPercentage={20} // number that represent the % of with that legend going to use.
                    legendFont={{
                        family: 'Arial',
                        size: 12,
                        color: '#000',
                        weight: 'bold',
                    }}
                    valueFont={{
                        family: 'Arial',
                        size: 12,
                        color: '#fff',
                        weight: 'bold',
                    }}
                    labelFont={{
                        family: 'Arial',
                        size: 16,
                        color: '#fff',
                        weight: 'bold',
                    }}
                    data={dog_temperament}
                />
                <h2>
                    Top 10 characteristics cats have for adoption
                </h2>
                <BubbleChart
                    graph={{
                        zoom: 1.1,
                    }}
                    width={1000}
                    height={1000}
                    padding={0} // optional value, number that set the padding between bubbles
                    showLegend={true} // optional value, pass false to disable the legend.
                    legendPercentage={20} // number that represent the % of with that legend going to use.
                    legendFont={{
                        family: 'Arial',
                        size: 12,
                        color: '#000',
                        weight: 'bold',
                    }}
                    valueFont={{
                        family: 'Arial',
                        size: 12,
                        color: '#fff',
                        weight: 'bold',
                    }}
                    labelFont={{
                        family: 'Arial',
                        size: 16,
                        color: '#fff',
                        weight: 'bold',
                    }}
                    data={cat_temperament}
                />
            </div>
        )
    }
}