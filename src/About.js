import React from "react";
import Button from "@material-ui/core/Button";
import { Helmet } from 'react-helmet'
import Favicon from 'react-favicon';

const TITLE = 'Book Master: About'

export default class About extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            canShow: false,
            data: ''
        }
        this.showTestResult = this.showTestResult.bind(this)
    }

    showTestResult() {
        if (this.state.canShow) {
            this.setState({
                canShow: false,
                data: ''
            })
        } else {
            fetch('/api/testcase').then(r => r.json()).then(data => {
                this.setState({
                    canShow: 1,
                    data: data
                })
            })
        }
    }

    render() {
        const {data, canShow} = this.state
        return (
            <div class="background-green aboutpadding">
				<Helmet>
				  <title>{ TITLE }</title>
				</Helmet>
				<div>
				  <Favicon url="https://i.imgur.com/eoGvMtb.png" />
				</div>
                <div>
                    <h1 className='text-center'><b>Meet The Team (Group 8):</b></h1>
                    <h5 className='text-center'>Hello there! We are Connor Kounthapanya, George Zhang, Tate Song, Ye
                        Sheng, and Yin Deng, and we
                        are the proud developers behind Book Master. We hope you enjoy your stay. We included a little
                        snippet of ourselves
                        and our roles on developing this website. We also included a lot of statistics and information
                        related to the development of this
                        website, including our GitLab code/wiki, Postman API, API for our book data, and the tools used
                        to build this website.</h5>
                    <div class="card-deck">
                        <div class="card">
                            <img className="img" src="https://i.imgur.com/WP65DYa.png" alt="Pic Not Found"
                                 style={{maxHeight: '100%', maxWidth: '100%'}}/>
                            <div class="card-body background-pink">
                                <h5 class="card-title"><b>Connor Kounthapanya</b></h5>
                                <p class="card-text"><b>Bio:</b> Connor is a sophomore Computer Science major at the
                                    University of
                                    Texas at Austin. He plans to graduate in May 2023. His hobbies include competitive
                                    programming, spending time with friends, and reading. He is very interested in
                                    software
                                    development and would like to work as a software engineer after graduation.</p>
                                <p class="card-text"><b>Major Responsibilties:</b> Back-End </p>
                                <p class="card-text"><b>No. of commits:</b> 38</p>
                                <p class="card-text"><b>No. of issues:</b> 18</p>
                                <p class="card-text"><b>No. of unit tests:</b> 9</p>
                            </div>
                        </div>

                        <div class="card">
                            <img className="img" src="https://i.imgur.com/lBZjMHH.jpg" alt="Pic Not Found"
                                 style={{maxHeight: '100%', maxWidth: '100%'}}/>
                            <div class="card-body background-pink">
                                <h5 class="card-title"><b>George Zhang</b></h5>
                                <p class="card-text"><b>Bio:</b> George is a rising senior studying Computer Science and
                                    Mathematics at UT Austin. In his free time, he enjoys watching TV and movies and
                                    listening/producing music. </p>
                                <p class="card-text"><b>Major Responsibilties:</b> Full-Stack, Hosting</p>
                                <p class="card-text"><b>No. of commits:</b> 46</p>
                                <p class="card-text"><b>No. of issues:</b> 6</p>
                                <p class="card-text"><b>No. of unit tests:</b> 3</p>
                            </div>
                        </div>

                        <div class="card">
                            <img className="img" src="https://i.imgur.com/okYv1ui.png" alt="Pic Not Found"
                                 style={{maxHeight: '100%', maxWidth: '100%'}}/>
                            <div class="card-body background-pink">
                                <h5 class="card-title"><b>Tate Song</b></h5>
                                <p class="card-text"><b>Bio:</b> Tate is a junior studying computer science at The
                                    University of
                                    Texas
                                    at Austin and exploring interests in theoretical computer science, systems and
                                    software
                                    development. In his free time, he likes to play games and eat food.</p>
                                <p class="card-text"><b>Major Responsibilties:</b> Back-End</p>
                                <p class="card-text"><b>No. of commits:</b> 13</p>
                                <p class="card-text"><b>No. of issues:</b> 8</p>
                                <p class="card-text"><b>No. of unit tests:</b> 3</p>
                            </div>
                        </div>

                        <div class="card">
                            <img className="img" src="https://i.imgur.com/jRRFIGs.jpg" alt="Pic Not Found"
                                 style={{maxHeight: '100%', maxWidth: '100%'}}/>
                            <div class="card-body background-pink">
                                <h5 class="card-title"><b>Ye Sheng</b></h5>
                                <p class="card-text"><b>Bio:</b> I'm a fourth-year CS student at UT Austin! I love
                                    reading young
                                    adult
                                    novels, watching Twitch, and playing video games. </p>
                                <p class="card-text"><b>Major Responsibilties:</b> Front-End</p>
                                <p class="card-text"><b>No. of commits:</b> 24</p>
                                <p class="card-text"><b>No. of issues:</b> 7</p>
                                <p class="card-text"><b>No. of unit tests:</b> 4</p>
                            </div>
                        </div>

                        <div class="card">
                            <img className="img" src="https://i.imgur.com/sJZ2YCS.jpg" alt="Pic Not Found"
                                 style={{maxHeight: '100%', maxWidth: '100%'}}/>
                            <div class="card-body background-pink">
                                <h5 class="card-title"><b>Yin Deng</b></h5>
                                <p class="card-text"><b>Bio:</b> Yin is a senior Computer Science student at UT Austin.
                                    He loves
                                    software development and computer system. In this spare time, he likes to learn new
                                    stuffs, include but not limit to technology, instruments, and feeding cats.</p>
                                <p class="card-text"><b>Major Responsibilties:</b> Front-End</p>
                                <p class="card-text"><b>No. of commits:</b> 38</p>
                                <p class="card-text"><b>No. of issues:</b> 16</p>
                                <p class="card-text"><b>No. of unit tests:</b> 3</p>
                            </div>
                        </div>
                    </div>
                    <br/>
                </div>

                <h1 className='text-center'><b>Statistics:</b></h1>
                <h5 className='text-center'>Here are some fun statistics about our website development.</h5>
                <div class="card-deck">
                    <div class="card">
                        <div class="card-body background-pink">
                            <p class="card-text"><b>Total no. of commits:</b> 159</p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body background-pink">
                            <p class="card-text"><b>Total no. of issues:</b> 55</p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body background-pink">
                            <p class="card-text"><b>Total no. of unit tests:</b> 22</p>
                        </div>
                    </div>
                </div>
                <br/>
                <br/>

                <h1 className='text-center'><b>Links:</b></h1>
                <h5 className='text-center'>Here are some links that may be useful for developers and programmers.</h5>
                <div class="card-deck">
                    <div class="card">
                        <div class="card-body background-pink">
                            <a href="https://documenter.getpostman.com/view/11803036/T17KcmHJ?version=latest"><b>Our
                                Postman
                                API</b></a>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body background-pink">
                            <a href="https://gitlab.com/ydeng2012/cs373-idb/-/issues"><b>GitLab Issue Tracker</b></a>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body background-pink">
                            <a href="https://gitlab.com/ydeng2012/cs373-idb"><b>GitLab Repo</b></a>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body background-pink">
                            <a href="https://gitlab.com/ydeng2012/cs373-idb/-/wikis/home"><b>GitLab Wiki</b></a>
                        </div>
                    </div>
					<div class="card">
                        <div class="card-body background-pink">
                            <a href="https://speakerdeck.com/gzhang1218/bookmaster"><b>Speaker Deck</b></a>
                        </div>
                    </div>
                </div>

                <br/>
                <br/>

                <h1 className='text-center'><b>Data:</b></h1>
                <h5 className='text-center'>Do you wonder where we got all the data for all our books? Well look no
                    further,
                    as almost all information on Book Master extracts data from these three APIs. </h5>
                <div class="card-deck">
                    <div class="card">
                        <div class="card-body background-pink">
                            <a href="https://www.goodreads.com/api"> <b>Goodreads API</b></a>
                            <p class="card-text">We use book, cover, and author. Developer Key Required. </p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body background-pink">
                            <a href="https://openlibrary.org/developers/api"><b>Open Library API</b></a>
                            <p class="card-text">We use book, ISBN, and covers. </p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body background-pink">
                            <a href="https://developers.google.com/books"><b>Google Books API</b></a>
                            <p class="card-text">We use book, cover image, and language. </p>
                        </div>
                    </div>
                </div>

                <br/>
                <br/>

                <h1 className='text-center'><b>Tools:</b></h1>
                <h5 className='text-center'>We have included a list of the tools we used to help create Book
                    Master. </h5>
                <div class="card-deck">
                    <div class="card">
                        <div class="card-body background-pink">
                            <p class="card-text"><b>Python: </b><br/>Used to run the main file.</p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body background-pink">
                            <p class="card-text"><b>JavaScript: </b><br/> Language for front-end.</p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body background-pink">
                            <p class="card-text"><b>React: </b><br/>Used for front-end UI.</p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body background-pink">
                            <p class="card-text"><b>BootStrap: </b><br/>Used for front-end CSS.</p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body background-pink">
                            <p class="card-text"><b>PostMan: </b><br/>Used for our API.</p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body background-pink">
                            <p class="card-text"><b>GCP: </b><br/>Used to deploy website</p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body background-pink">
                            <p class="card-text"><b>HTML: </b><br/>Language for front-end.</p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body background-pink">
                            <p class="card-text"><b>PostgreSQL: </b><br/>Database used for back-end.</p>
                        </div>
                    </div>
                </div>

                <br/>
                <br/>

                <div className="text-center">
                    <h1 className='text-center'><b> Miscellaneous: </b></h1>
                    <Button variant="contained" color="secondary" onClick={this.showTestResult}>
                        Unittest Button
                    </Button>
                    {canShow && <div>{data}</div>}
                    <br/>
                    <br/>
                </div>
            </div>
        )
    }
}