import React from 'react'
import {Link} from "react-router-dom";
import { Helmet } from 'react-helmet'
import Favicon from 'react-favicon';

const TITLE = 'Book Master: Book Details'

export default class BookDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            book: {},
            authors: [],
            publisher: {},
        }
    }

    componentDidMount() {
        Promise.all([fetch('/api/data'), fetch('/api/publisher'), fetch('/api/author')])
            .then(([res1, res2, res3]) => {
                return Promise.all([res1.json(), res2.json(), res3.json()])
            })
            .then(([data, publisher, author]) => {
                // for book
                for (let item of data.books) {
                    if (item.id === parseInt(this.props.match.params.id)) {
                        this.setState({book: item})
                    }
                }
                // for publisher
                for (let pub of publisher.publishers) {
                    if (pub.name === this.state.book.publisher) {
                        this.setState({publisher: pub})
                    }
                }
                // for author
                let res = []
                for (let aut of author.authors) {
                    if (aut.author === this.state.book.author) {
                        res.push(aut)
                    }
                }
                this.setState({authors: res})
            })
    }

    render() {
        const {book, authors, publisher} = this.state
        return (
            <div class='container'>
				<Helmet>
				  <title>{ TITLE }</title>
				</Helmet>
				<div>
				  <Favicon url="https://i.imgur.com/eoGvMtb.png" />
				</div>
                <div class="row">
                    <div class="p-2 flex-fill bd-highlight">
                        <div class="card mb-3">
                            <div class="row no-gutters">
                                <div class="col-md-4">
                                    <img src={book.src} class="card-img" alt="Does Not Exist"
                                         style={{maxHeight: '100%', maxWidth: '100%'}}/>
                                </div>
                                <div class="col-md-8">
                                    <div class="card-body">
                                        <h1 class="card-title"><b>Book: {book.title}</b></h1>
                                        <p class="card-text"><b>Description:</b> {book.summary} </p>
                                        <p class="card-text"><b>Author:</b> {book.author} </p>
                                        <p class="card-text"><b>ISBN:</b> {book.ISBN} </p>
                                        <p class="card-text"><b>Release Date:</b> {book.publish_date} </p>
                                        <p class="card-text"><b>Publisher:</b> {book.publisher} </p>
                                        <p class="card-text"><b>Number of Pages:</b> {book.page} </p>
                                        <p class="card-text"><b>Language:</b> {book.language} </p>
                                        <div style={{display: 'flex', justifyContent: 'center'}}>
                                            <a href={book.link} class="btn btn-primary">For More Information</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="p-2 flex-fill bd-highlight">
                        <div class="card-deck">
                            <div class="card">
                                {authors.map((aut, i) =>
                                    <div key={i}>
                                        <Link to={{
                                            pathname: `/authors/authordetail/${aut.author}`,
                                        }}>
                                            <h1><b>Author: {aut.author}</b></h1>
                                        </Link>
                                        <p class="card-text"><b>Birthday:</b> {aut.birthday} </p>
                                        <p class="card-text"><b>Hometown:</b> {aut.hometown} </p>
                                        <p class="card-text"><b>Education:</b> {aut.education} </p>
                                        <p class="card-text"><b>Known For:</b> {aut.works} </p>
                                        <div style={{display: 'flex', justifyContent: 'center'}}>
                                            <a href={aut.link} class="btn btn-primary">Personal Website</a>
                                        </div>
                                    </div>
                                )}
                            </div>
                            <div class="card">
                                <Link to={{
                                    pathname: `/publishers/publisherdetail/${publisher.name}`,
                                }}>
                                    <h1><b>Publisher: {publisher.name}</b></h1>
                                </Link>
                                <p class="card-text"><b>Phone:</b> {publisher.phone} </p>
                                <p class="card-text"><b>City:</b> {publisher.location} </p>
                                <p class="card-text"><b>Founded:</b> {publisher.date} </p>
                                <p class="card-text"><b>Founder:</b> {publisher.founder} </p>
                                <div style={{display: 'flex', justifyContent: 'center'}}>
                                    <a href={publisher.link} class="btn btn-primary">Personal Website</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}