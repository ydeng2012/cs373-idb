import React from 'react'
import {Link} from "react-router-dom";
import { Helmet } from 'react-helmet'
import Favicon from 'react-favicon';

const TITLE = 'Book Master: Publisher Details'

export default class PublisherDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            publisher: {},
            books: [],
            authors: [],
        }
    }

    componentDidMount() {
        // publisher also need to see all books that have same publisher name
        // we actually need to get all books which has the this company name
        Promise.all([fetch('/api/publisher'), fetch('/api/data'), fetch('/api/author/')])
            .then(([res1, res2, res3]) => {
                return Promise.all([res1.json(), res2.json(), res3.json()])
            })
            .then(([pub, data, aut]) => {
                // for publishers
                for (let item of pub.publishers) {
                    if (item.name === this.props.match.params.id) {
                        this.setState({publisher: item})
                    }
                }
                // for books
                let book_list = []
                for (let book of data.books) {
                    if (book.publisher === this.state.publisher.name) {
                        book_list.push(book)
                    }
                }
                this.setState({books: book_list})
                // for authors
                let author_list = []
				let no_duplicate = []
                for (let author of aut.authors) {
                    for (let book of book_list) {
                        if (book.author === author.author) {
							var dup = (no_duplicate.indexOf((book.author).toString()) > -1)
							if (dup === false){
								no_duplicate.push((book.author).toString())
								author_list.push(author)
							}
                        }
                    }
                }
                this.setState({authors: author_list})
            })
    }

    render() {
        const {publisher, books, authors} = this.state
        return (
            <div class='container'>
				<Helmet>
				  <title>{ TITLE }</title>
				</Helmet>
				<div>
				  <Favicon url="https://i.imgur.com/eoGvMtb.png" />
				</div>
                <div class="row">
                    <div class="p-2 flex-fill bd-highlight">
                        <div class="card mb-3">
                            <div class="row no-gutters">
                                <div class="col-md-4">
                                    <img src={publisher.src} class="card-img" alt="Does Not Exist"
                                         style={{maxHeight: '100%', maxWidth: '100%'}}/>
                                </div>
                                <div class="col-md-8">
                                    <div class="card-body">
                                        <h1 class="card-title"><b>Publisher: {publisher.name}</b></h1>
                                        <p class="card-text"><b>Phone: {publisher.phone}</b></p>
                                        <p class="card-text"><b>Location:</b> {publisher.location} </p>
                                        <p class="card-text"><b>Date:</b> {publisher.date} </p>
                                        <p class="card-text"><b>Founder:</b> {publisher.founder} </p>
                                        <div style={{display: 'flex', justifyContent: 'center'}}>
                                            <a href={publisher.link} class="btn btn-primary">For More Information</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="p-2 flex-fill bd-highlight">
                        <div class="card-deck">
                            <div class="card">
                                {books.map((book, i) =>
                                    <div key={i}>
                                        <Link to={{
                                            pathname: `/books/bookdetail/${book.id}`,
                                        }}>
                                            <h1><b>Book: {book.title}</b></h1>
                                        </Link>
                                        <p class="card-text"><b>Description:</b> {book.summary} </p>
                                        <p class="card-text"><b>Author:</b> {book.author} </p>
                                        <p class="card-text"><b>ISBN:</b> {book.ISBN} </p>
                                        <p class="card-text"><b>Release Date:</b> {book.publish_date} </p>
                                        <p class="card-text"><b>Publisher:</b> {book.publisher} </p>
                                        <p class="card-text"><b>Number of Pages:</b> {book.page} </p>
                                        <p class="card-text"><b>Language:</b> {book.language} </p>
                                        <div style={{display: 'flex', justifyContent: 'center'}}>
                                            <a href={book.link} class="btn btn-primary">Book Website</a>
                                        </div>
                                    </div>
                                )}
                            </div>
                            <div class="card">
                                {authors.map((aut, i) =>
                                    <div key={i}>
                                        <Link to={{
                                            pathname: `/authors/authordetail/${aut.author}`,
                                        }}>
                                            <h1><b>Author: {aut.author}</b></h1>
                                        </Link>
                                        <p class="card-text"><b>Birthday:</b> {aut.birthday} </p>
                                        <p class="card-text"><b>Hometown:</b> {aut.hometown} </p>
                                        <p class="card-text"><b>Education:</b> {aut.education} </p>
                                        <p class="card-text"><b>Known For:</b> {aut.works} </p>
                                        <div style={{display: 'flex', justifyContent: 'center'}}>
                                            <a href={aut.link} class="btn btn-primary">Personal Website</a>
                                        </div>
                                    </div>
                                )}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}