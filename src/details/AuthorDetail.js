import React from 'react'
import {Link} from "react-router-dom";
import { Helmet } from 'react-helmet'
import Favicon from 'react-favicon';

const TITLE = 'Book Master: Author Details'

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            author: {},
            books: [],
            publishers: [],
        }
    }

    componentDidMount() {
        // subject to change here, we can't query the whole database everytime,
        // everytime we want to grab just books and publishers that matches
        // our authors
        Promise.all([fetch('/api/author'), fetch('/api/data'), fetch('/api/publisher')])
            .then(([res1, res2, res3]) => {
                return Promise.all([res1.json(), res2.json(), res3.json()])
            })
            .then(([aut, data, pub]) => {
                // for author
                for (let item of aut.authors) {
                    if (item.author === this.props.match.params.id) {
                        this.setState({author: item})
                    }
                }
                // for books
                let book_list = []
                for (let book of data.books) {
                    if (book.author === this.state.author.author) {
                        book_list.push(book)
                    }
                }
                this.setState({books: book_list})
                // for publishers
                let publisher_list = []
				let no_duplicate = []
                for (let publisher of pub.publishers) {
                    for (let book of book_list) {
                        if (book.publisher === publisher.name) {
							var dup = (no_duplicate.indexOf((book.publisher).toString()) > -1)
							if (dup === false){
								no_duplicate.push((publisher.name).toString())
								publisher_list.push(publisher)
							}
                        }
                    }
                }
                this.setState({publishers: publisher_list})
            })
    }

    render() {
        const {author, books, publishers} = this.state
        return (
            <div class='container'>
				<Helmet>
				  <title>{ TITLE }</title>
				</Helmet>
				<div>
				  <Favicon url="https://i.imgur.com/eoGvMtb.png" />
				</div>
                <div class="row">
                    <div class="p-2 flex-fill bd-highlight">
                        <div class="card mb-3">
                            <div class="row no-gutters">
                                <div class="col-md-4">
                                    <img src={author.src} class="card-img" alt="Does Not Exist"
                                         style={{maxHeight: '100%', maxWidth: '100%'}}/>
                                </div>
                                <div class="col-md-8">
                                    <div class="card-body">
                                        <h1 class="card-title"><b>Author: {author.author}</b></h1>
                                        <p class="card-text"><b>Birthday:</b> {author.birthday} </p>
                                        <p class="card-text"><b>Hometown:</b> {author.hometown} </p>
                                        <p class="card-text"><b>Education:</b> {author.education} </p>
                                        <div style={{display: 'flex', justifyContent: 'center'}}>
                                            <a href={author.link} class="btn btn-primary">For More Information</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="p-2 flex-fill bd-highlight">
                        <div class="card-deck">
                            <div class="card">
                                {books.map((book, i) =>
                                    <div key={i}>
                                        <Link to={{
                                            pathname: `/books/bookdetail/${book.id}`,
                                        }}>
                                            <h1><b>Book: {book.title}</b></h1>
                                        </Link>
                                        <p class="card-text"><b>Description:</b> {book.summary} </p>
                                        <p class="card-text"><b>Author:</b> {book.author} </p>
                                        <p class="card-text"><b>ISBN:</b> {book.ISBN} </p>
                                        <p class="card-text"><b>Release Date:</b> {book.publish_date} </p>
                                        <p class="card-text"><b>Publisher:</b> {book.publisher} </p>
                                        <p class="card-text"><b>Number of Pages:</b> {book.page} </p>
                                        <p class="card-text"><b>Language:</b> {book.language} </p>
                                        <div style={{display: 'flex', justifyContent: 'center'}}>
                                            <a href={book.link} class="btn btn-primary">Book Website</a>
                                        </div>
                                    </div>
                                )}
                            </div>
                            <div class="card">
                                {publishers.map((publisher, i) =>
                                    <div key={i}>
                                        <Link to={{
                                            pathname: `/publishers/publisherdetail/${publisher.name}`,
                                        }}>
                                            <h1><b>Publisher: {publisher.name}</b></h1>
                                        </Link>
                                        <p class="card-text"><b>Phone:</b> {publisher.phone} </p>
                                        <p class="card-text"><b>City:</b> {publisher.location} </p>
                                        <p class="card-text"><b>Founded:</b> {publisher.date} </p>
                                        <p class="card-text"><b>Founder:</b> {publisher.founder} </p>
                                        <div style={{display: 'flex', justifyContent: 'center'}}>
                                            <a href={publisher.link} class="btn btn-primary">Personal Website</a>
                                        </div>
                                    </div>
                                )}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}