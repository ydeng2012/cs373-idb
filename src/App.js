import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Navigation from "./Navigation";
import {Switch, Route} from "react-router-dom";
import Books from "./Books";
import Publishers from "./Publishers";
import Authors from "./Authors";
import About from "./About";
import Landing from "./Landing";
import BookDetail from "./details/BookDetail";
import AuthorDetail from "./details/AuthorDetail";
import PublisherDetail from "./details/PublisherDetail";
import Visualization from './Visualization'
import Search from './Search'

// import {Container} from "@material-ui/core";

class App extends React.Component {

    render() {
        return (
            <div>
                <Navigation/>
                <Switch>
                    <Route path="/books/bookdetail/:id" component={BookDetail}/>
                    <Route path="/authors/authordetail/:id" component={AuthorDetail}/>
                    <Route path="/publishers/publisherdetail/:id" component={PublisherDetail}/>
                    <Route path="/visualization" component={Visualization}/>
                    <Route path="/books" component={Books}/>
                    <Route path="/publishers" component={Publishers}/>
                    <Route path="/authors" component={Authors}/>
					<Route path="/search" component={Search}/>
                    <Route path="/about" component={About}/>
                    <Route path="/" component={Landing}/>
                </Switch>
            </div>
        )
    }
}

export default App;
