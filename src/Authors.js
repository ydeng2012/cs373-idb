import React from 'react'
import {Link} from "react-router-dom";
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import { Helmet } from 'react-helmet'
import Favicon from 'react-favicon';

const TITLE = 'Book Master: Authors'

class Authors extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            authors: []
        }
    }

    componentDidMount() {
        // fetch send the request to flask api because of proxy config
        // we send directly to port 5000
        // then return a promise
        fetch('/api/author').then(r => r.json()).then(author => {
            this.setState({authors: author.authors})
        })
    }

    render() {
		function imageFormatter(cell, row) {
			return (
			<td style={{
				width: "200px",
				height: "200px",
				backgroundColorlor: "white",
				textAlign: "center",
				verticalAlign: "Alimiddle"
			}}>
				<img className="img" src={ cell } alt={"Image Not Found"}
					style={{maxHeight: '100%', maxWidth: '100%'}}/>
			</td>
									
			);
		}
		
		function authorDetail(cell, row) {
			return (
					<Link to={{
						pathname: `/authors/authordetail/${row.author}`,
					}}>{ cell }</Link>				
			);
		}
		
        const {authors} = this.state
		const {columns} = {columns: [{
			  dataField: 'author',
			  text: 'Author',
			  sort: true,
			  formatter: authorDetail
			}, {
			  dataField: 'src',
			  text: 'Image',
			  sort: true,
			  formatter: imageFormatter,
			  searchable:false
			}, {
			  dataField: 'birthday',
			  text: 'Birthday',
			  sort: true,
			  searchable:false
			}, {
			  dataField: 'hometown',
			  text: 'Hometown',
			  sort: true,
			  searchable:false
			}, {
			  dataField: 'education',
			  text: 'Education',
			  sort: true,
			  searchable:false
			}, {
			  dataField: 'works',
			  text: 'Known For',
			  sort: true,
			  searchable:false
			}, {
			  dataField: 'link',
			  text: 'Personal Website',
			  sort: true,
			  hidden: true,
			  searchable:false
			}, {
			  dataField: 'id',
			  text: 'ID',
			  sort: true,
			  hidden: true,
			  searchable:false
			}		
		]}
		const { SearchBar, ClearSearchButton } = Search
        return (
            <div>
				<Helmet>
				  <title>{ TITLE }</title>
				</Helmet>
				<div>
				  <Favicon url="https://i.imgur.com/eoGvMtb.png" />
				</div>
                <h1 className="text-center">Authors</h1>
				<h5 className="text-center">Hint: You can click on the column names to sort by that column information up or down alphanumerically.</h5>
                <div class="card-deck aboutcard">
					<div class="card">
						<div class="card-body">
							<a href="/books">Books</a>
						</div>
					</div>
					<div class="card">
						<div class="card-body">
							<a href="/publishers">Publishers</a>
						</div>
					</div>
					<div class="card">
						<div class="card-body">						
							<p><b>Authors</b></p>
						</div>
					</div>
				</div>
				<br/>
				<ToolkitProvider
				  keyField="id"
				  data={ authors }
				  columns={ columns }
				  search
				  >
				  {
					props => (
					  <div>
						<div style={{display: 'flex', justifyContent: 'center'}}>
							<SearchBar { ...props.searchProps }/>
							<ClearSearchButton { ...props.searchProps } />
						</div> 
						<hr/>
						<BootstrapTable
							{...props.baseProps }
							striped
							pagination={ paginationFactory() }
						/>
					  </div>
					)
				  }
				</ToolkitProvider>
            </div>
        )
    }
}

export default Authors;

